package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 入库质检单明细
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
@TableName("w_inbound_inspect_line")
public class InboundInspectLine extends Model<InboundInspectLine> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 质检单头键
     */
    @TableField("header_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long headerId;
    /**
     * 序号
     */
    @TableField("notice_line_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long noticeLineId;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 物料ID
     */
    @TableField("materiel_id")
    private String materielId;
    /**
     * 物料代码
     */
    @TableField("materiel_code")
    private String materielCode;
    /**
     * 物料名称
     */
    @TableField("materiel_name")
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 收货数量
     */
    @TableField("recv_qty")
    private BigDecimal recvQty;
    /**
     * 收货净重
     */
    @TableField("recv_net_weight")
    private BigDecimal recvNetWeight;
    /**
     * 收货毛重
     */
    @TableField("recv_gross_weight")
    private BigDecimal recvGrossWeight;
    /**
     * 收货体积
     */
    @TableField("recv_gross_cubage")
    private BigDecimal recvGrossCubage;
    /**
     * 收货件数
     */
    @TableField("recv_packed_count")
    private BigDecimal recvPackedCount;
    /**
     * 合格数量
     */
    @TableField("qualified_qty")
    private BigDecimal qualifiedQty;
    /**
     * 合格净重
     */
    @TableField("qualified_net_weight")
    private BigDecimal qualifiedNetWeight;
    /**
     * 合格毛重
     */
    @TableField("qualified_gross_weight")
    private BigDecimal qualifiedGrossWeight;
    /**
     * 合格体积
     */
    @TableField("qualified_gross_cubage")
    private BigDecimal qualifiedGrossCubage;
    /**
     * 合格件数
     */
    @TableField("qualified_packed_count")
    private BigDecimal qualifiedPackedCount;
    /**
     * 破损数量
     */
    @TableField("damaged_qty")
    private BigDecimal damagedQty;
    /**
     * 破损净重
     */
    @TableField("damaged_net_weight")
    private BigDecimal damagedNetWeight;
    /**
     * 破损毛重
     */
    @TableField("damaged_gross_weight")
    private BigDecimal damagedGrossWeight;
    /**
     * 破损体积
     */
    @TableField("damaged_gross_cubage")
    private BigDecimal damagedGrossCubage;
    /**
     * 破损件数
     */
    @TableField("damaged_packed_count")
    private BigDecimal damagedPackedCount;
    /**
     * 批号0
     */
    @TableField("lot_no0")
    private String lotNo0;
    /**
     * 批号1
     */
    @TableField("lot_no1")
    private String lotNo1;
    /**
     * 批号2
     */
    @TableField("lot_no2")
    private String lotNo2;
    /**
     * 批号3
     */
    @TableField("lot_no3")
    private String lotNo3;
    /**
     * 批号4
     */
    @TableField("lot_no4")
    private String lotNo4;
    /**
     * 批号5
     */
    @TableField("lot_no5")
    private String lotNo5;
    /**
     * 批号6
     */
    @TableField("lot_no6")
    private String lotNo6;
    /**
     * 批号7
     */
    @TableField("lot_no7")
    private String lotNo7;
    /**
     * 批号8
     */
    @TableField("lot_no8")
    private String lotNo8;
    /**
     * 批号9
     */
    @TableField("lot_no9")
    private String lotNo9;
    /**
     * 状态(10:全部合格,20:部分合格,30:全部破损)
     */
    private String status;
    /**
     * 状态(10:全部入库,20:合格合格,30:拒绝入库)
     */
    @TableField("deal_method")
    private String dealMethod;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public Long getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(Long noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getRecvQty() {
        return recvQty;
    }

    public void setRecvQty(BigDecimal recvQty) {
        this.recvQty = recvQty;
    }

    public BigDecimal getRecvNetWeight() {
        return recvNetWeight;
    }

    public void setRecvNetWeight(BigDecimal recvNetWeight) {
        this.recvNetWeight = recvNetWeight;
    }

    public BigDecimal getRecvGrossWeight() {
        return recvGrossWeight;
    }

    public void setRecvGrossWeight(BigDecimal recvGrossWeight) {
        this.recvGrossWeight = recvGrossWeight;
    }

    public BigDecimal getRecvGrossCubage() {
        return recvGrossCubage;
    }

    public void setRecvGrossCubage(BigDecimal recvGrossCubage) {
        this.recvGrossCubage = recvGrossCubage;
    }

    public BigDecimal getRecvPackedCount() {
        return recvPackedCount;
    }

    public void setRecvPackedCount(BigDecimal recvPackedCount) {
        this.recvPackedCount = recvPackedCount;
    }

    public BigDecimal getQualifiedQty() {
        return qualifiedQty;
    }

    public void setQualifiedQty(BigDecimal qualifiedQty) {
        this.qualifiedQty = qualifiedQty;
    }

    public BigDecimal getQualifiedNetWeight() {
        return qualifiedNetWeight;
    }

    public void setQualifiedNetWeight(BigDecimal qualifiedNetWeight) {
        this.qualifiedNetWeight = qualifiedNetWeight;
    }

    public BigDecimal getQualifiedGrossWeight() {
        return qualifiedGrossWeight;
    }

    public void setQualifiedGrossWeight(BigDecimal qualifiedGrossWeight) {
        this.qualifiedGrossWeight = qualifiedGrossWeight;
    }

    public BigDecimal getQualifiedGrossCubage() {
        return qualifiedGrossCubage;
    }

    public void setQualifiedGrossCubage(BigDecimal qualifiedGrossCubage) {
        this.qualifiedGrossCubage = qualifiedGrossCubage;
    }

    public BigDecimal getQualifiedPackedCount() {
        return qualifiedPackedCount;
    }

    public void setQualifiedPackedCount(BigDecimal qualifiedPackedCount) {
        this.qualifiedPackedCount = qualifiedPackedCount;
    }

    public BigDecimal getDamagedQty() {
        return damagedQty;
    }

    public void setDamagedQty(BigDecimal damagedQty) {
        this.damagedQty = damagedQty;
    }

    public BigDecimal getDamagedNetWeight() {
        return damagedNetWeight;
    }

    public void setDamagedNetWeight(BigDecimal damagedNetWeight) {
        this.damagedNetWeight = damagedNetWeight;
    }

    public BigDecimal getDamagedGrossWeight() {
        return damagedGrossWeight;
    }

    public void setDamagedGrossWeight(BigDecimal damagedGrossWeight) {
        this.damagedGrossWeight = damagedGrossWeight;
    }

    public BigDecimal getDamagedGrossCubage() {
        return damagedGrossCubage;
    }

    public void setDamagedGrossCubage(BigDecimal damagedGrossCubage) {
        this.damagedGrossCubage = damagedGrossCubage;
    }

    public BigDecimal getDamagedPackedCount() {
        return damagedPackedCount;
    }

    public void setDamagedPackedCount(BigDecimal damagedPackedCount) {
        this.damagedPackedCount = damagedPackedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDealMethod() {
        return dealMethod;
    }

    public void setDealMethod(String dealMethod) {
        this.dealMethod = dealMethod;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "InboundInspectLine{" +
                ", id=" + id +
                ", headerId=" + headerId +
                ", noticeLineId=" + noticeLineId +
                ", ownerId=" + ownerId +
                ", ownerOrderNo=" + ownerOrderNo +
                ", materielId=" + materielId +
                ", materielCode=" + materielCode +
                ", materielName=" + materielName +
                ", uom=" + uom +
                ", recvQty=" + recvQty +
                ", recvNetWeight=" + recvNetWeight +
                ", recvGrossWeight=" + recvGrossWeight +
                ", recvGrossCubage=" + recvGrossCubage +
                ", recvPackedCount=" + recvPackedCount +
                ", qualifiedQty=" + qualifiedQty +
                ", qualifiedNetWeight=" + qualifiedNetWeight +
                ", qualifiedGrossWeight=" + qualifiedGrossWeight +
                ", qualifiedGrossCubage=" + qualifiedGrossCubage +
                ", qualifiedPackedCount=" + qualifiedPackedCount +
                ", damagedQty=" + damagedQty +
                ", damagedNetWeight=" + damagedNetWeight +
                ", damagedGrossWeight=" + damagedGrossWeight +
                ", damagedGrossCubage=" + damagedGrossCubage +
                ", damagedPackedCount=" + damagedPackedCount +
                ", lotNo0=" + lotNo0 +
                ", lotNo1=" + lotNo1 +
                ", lotNo2=" + lotNo2 +
                ", lotNo3=" + lotNo3 +
                ", lotNo4=" + lotNo4 +
                ", lotNo5=" + lotNo5 +
                ", lotNo6=" + lotNo6 +
                ", lotNo7=" + lotNo7 +
                ", lotNo8=" + lotNo8 +
                ", lotNo9=" + lotNo9 +
                ", status=" + status +
                ", dealMethod=" + dealMethod +
                ", remarks=" + remarks +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
