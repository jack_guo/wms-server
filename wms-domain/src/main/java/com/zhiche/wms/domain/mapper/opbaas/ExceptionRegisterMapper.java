package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExceptionRegisterDetailDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 异常登记 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface ExceptionRegisterMapper extends BaseMapper<ExceptionRegister> {

    List<ExceptionRegisterDetailDTO> countExDetail(HashMap<Object, Object> params);

    List<ExResultDTO> queryExDetail(HashMap<String, Object> params);

    void updateSQLMode();

    void deleteExRegister(HashMap<Object, Object> params);

    List<ExResultDTO> selectExDetailPage(Page<ExResultDTO> page, @Param("ew") Wrapper<ExResultDTO> ew);

    int countExceptionTotal(@Param("ew") Wrapper<Integer> ew);

    List<ExResultDTO> queryExceptionDetailsByPage(@Param("pageMap") HashMap<String, Object> pageMap, @Param("ew") Wrapper<Integer> ew);

    List<ExResultDTO> queryExportExcpList(@Param("ew") Wrapper<ExResultDTO> ew);

    List<ExResultDTO> selectExListDetail(Page<ExResultDTO> page, @Param("ew") Wrapper<ExResultDTO> ew);
}
