package com.zhiche.wms.domain.mapper.stockinit;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.stockinit.StockInitLine;
import com.zhiche.wms.dto.stockinit.StockInitQueryListResultDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 期初库存明细 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface StockInitLineMapper extends BaseMapper<StockInitLine> {

    List<StockInitQueryListResultDTO> queryInitPage(Page<StockInitQueryListResultDTO> page,
                                                    @Param("ew") Wrapper<StockInitQueryListResultDTO> wp);
}
