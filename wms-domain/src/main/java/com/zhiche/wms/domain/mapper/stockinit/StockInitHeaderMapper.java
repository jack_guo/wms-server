package com.zhiche.wms.domain.mapper.stockinit;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.stockinit.StockInitHeader;

import java.util.HashMap;

/**
 * <p>
 * 期初库存头 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface StockInitHeaderMapper extends BaseMapper<StockInitHeader> {

    int countInitStockByParam(HashMap<String, Object> params);
}
