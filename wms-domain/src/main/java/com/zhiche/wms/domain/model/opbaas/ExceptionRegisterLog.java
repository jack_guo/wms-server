package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:18 2019/1/11
 */
@TableName("exceptionregister_test_log")
public class ExceptionRegisterLog extends Model<ExceptionRegisterLog> {

    private String vin;
    private String remark;
    private String type;

    @TableField("gmt_create")
    private Date gmtCreate;

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("ExceptionRegisterLog{");
        sb.append("vin='").append(vin).append('\'');
        sb.append(", remark='").append(remark).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", gmtCreate='").append(gmtCreate).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    protected Serializable pkVal () {
        return null;
    }
}
