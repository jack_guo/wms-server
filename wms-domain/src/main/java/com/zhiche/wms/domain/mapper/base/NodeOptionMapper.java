package com.zhiche.wms.domain.mapper.base;

import com.zhiche.wms.domain.model.base.NodeOption;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 仓库可选节点 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
public interface NodeOptionMapper extends BaseMapper<NodeOption> {

}
