package com.zhiche.wms.domain.model.log;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 接口导出日志头
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@TableName("w_itf_explog_header")
public class ItfExplogHeader extends Model<ItfExplogHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<ItfExplogLine> itfExplogLines;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 导出类型(10:入库导出,20:出库导出)
     */
    @TableField("export_type")
    private String exportType;
    /**
     * 相关单据ID
     */
    @TableField("relation_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long relationId;
    /**
     * 目标系统(10:ILS)
     */
    @TableField("target_source")
    private String targetSource;
    /**
     * 导出次数
     */
    @TableField("exp_count")
    private Integer expCount;
    /**
     * 导出状态(1:成功,0:失败)
     */
    @TableField("export_status")
    private String exportStatus;
    /**
     * 导出备注
     */
    @TableField("export_remarks")
    private String exportRemarks;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("request_id")
    private String requestId;


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public List<ItfExplogLine> getItfExplogLines() {
        return itfExplogLines;
    }

    public void setItfExplogLines(List<ItfExplogLine> itfExplogLines) {
        this.itfExplogLines = itfExplogLines;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExportType() {
        return exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public String getTargetSource() {
        return targetSource;
    }

    public void setTargetSource(String targetSource) {
        this.targetSource = targetSource;
    }

    public Integer getExpCount() {
        return expCount;
    }

    public void setExpCount(Integer expCount) {
        this.expCount = expCount;
    }

    public String getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(String exportStatus) {
        this.exportStatus = exportStatus;
    }

    public String getExportRemarks() {
        return exportRemarks;
    }

    public void setExportRemarks(String exportRemarks) {
        this.exportRemarks = exportRemarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ItfExplogHeader{" +
                ", id=" + id +
                ", exportType=" + exportType +
                ", relationId=" + relationId +
                ", targetSource=" + targetSource +
                ", expCount=" + expCount +
                ", exportStatus=" + exportStatus +
                ", exportRemarks=" + exportRemarks +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
