package com.zhiche.wms.domain.model.sys;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-17
 */
@TableName("w_app_version")
public class AppVersion extends Model<AppVersion> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 是否强制
     */
    @TableField("is_force")
    private Integer isForce;
    /**
     * 更新说明
     */
    @TableField("release_note")
    private String releaseNote;
    /**
     * 更新说明
     */
    @TableField("release_noteCN")
    private String releaseNoteCN;
    /**
     * 地址
     */
    private String url;
    /**
     * 版本号
     */
    private Integer versionCode;
    /**
     * 版本记录
     */
    private String version;
    /**
     * 扩展1
     */
    @TableField("ext_1")
    private String ext1;
    /**
     * 扩展2
     */
    @TableField("ext_2")
    private String ext2;
    /**
     * 扩展3
     */
    @TableField("ext_3")
    private String ext3;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_update")
    private Date gmtUpdate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsForce() {
        return isForce;
    }

    public void setIsForce(Integer isForce) {
        this.isForce = isForce;
    }

    public String getReleaseNote() {
        return releaseNote;
    }

    public void setReleaseNote(String releaseNote) {
        this.releaseNote = releaseNote;
    }

    public String getReleaseNoteCN() {
        return releaseNoteCN;
    }

    public void setReleaseNoteCN(String releaseNoteCN) {
        this.releaseNoteCN = releaseNoteCN;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2;
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppVersion{" +
                ", id=" + id +
                ", isForce=" + isForce +
                ", releaseNote=" + releaseNote +
                ", releaseNoteCN=" + releaseNoteCN +
                ", url=" + url +
                ", versionCode=" + versionCode +
                ", version=" + version +
                ", ext1=" + ext1 +
                ", ext2=" + ext2 +
                ", ext3=" + ext3 +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                "}";
    }
}
