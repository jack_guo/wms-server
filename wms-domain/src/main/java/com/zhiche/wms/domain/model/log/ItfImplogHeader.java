package com.zhiche.wms.domain.model.log;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 仓储接口导入日志头
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@TableName("w_itf_implog_header")
public class ItfImplogHeader extends Model<ItfImplogHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<ItfImplogLine> itfImplogLineList;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 订单类型(10:入库,20:出库,30:移库)
     */
    private String type;
    /**
     * 下单日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 发货仓库
     */
    @TableField("delivery_house_id")
    private String deliveryHouseId;
    /**
     * 发货仓库编码
     */
    @TableField("delivery_house_code")
    private String deliveryHouseCode;
    /**
     * 发货仓库名称
     */
    @TableField("delivery_house_name")
    private String deliveryHouseName;
    /**
     * 收货仓库
     */
    @TableField("recv_house_id")
    private String recvHouseId;
    /**
     * 收货仓库编码
     */
    @TableField("recv_house_code")
    private String recvHouseCode;
    /**
     * 收货仓库名称
     */
    @TableField("recv_house_name")
    private String recvHouseName;
    /**
     * 承运商
     */
    @TableField("carrier_id")
    private String carrierId;
    /**
     * 承运商名称
     */
    @TableField("carrier_name")
    private String carrierName;
    /**
     * 运输方式
     */
    @TableField("transport_method")
    private String transportMethod;
    /**
     * 车船号
     */
    @TableField("plate_number")
    private String plateNumber;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 预计出库日期
     */
    @TableField("expect_out_date_lower")
    private Date expectOutDateLower;
    /**
     * 预计出库时间上限
     */
    @TableField("expect_out_date_upper")
    private Date expectOutDateUpper;
    /**
     * 预计入库日期
     */
    @TableField("expect_in_date_lower")
    private Date expectInDateLower;
    /**
     * 预计入库时间上限
     */
    @TableField("expect_in_date_upper")
    private Date expectInDateUpper;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计数量
     */
    @TableField("expect_sum_qty")
    private BigDecimal expectSumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 来源系统(20:oms系统触发,30:tms系统触发,40:君马系统触发)
     */
    @TableField("source_system")
    private String sourceSystem;
    /**
     * 来源唯一键
     */
    @TableField("source_key")
    private String sourceKey;
    /**
     * 来源单据号
     */
    @TableField("source_no")
    private String sourceNo;
    /**
     * 状态(10:正常,20:关闭,30:取消)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 导入状态
     */
    @TableField("import_status")
    private String importStatus;
    /**
     * 导入备注
     */
    @TableField("import_remarks")
    private String importRemarks;
    /**
     * 导入时间(业务数据生成时间)
     */
    @TableField("import_time")
    private Date importTime;
    /**
     * 数据内容
     */
    @TableField("data_content")
    private String dataContent;
    /**
     * 用户自定义0
     */
    @TableField("user_def0")
    private String userDef0;
    /**
     * 用户自定义1
     */
    @TableField("user_def1")
    private String userDef1;
    /**
     * 用户自定义2
     */
    @TableField("user_def2")
    private String userDef2;
    /**
     * 用户自定义3
     */
    @TableField("user_def3")
    private String userDef3;
    /**
     * 用户自定义4
     */
    @TableField("user_def4")
    private String userDef4;
    /**
     * 用户自定义5
     */
    @TableField("user_def5")
    private String userDef5;
    /**
     * 用户自定义6
     */
    @TableField("user_def6")
    private String userDef6;
    /**
     * 用户自定义7
     */
    @TableField("user_def7")
    private String userDef7;
    /**
     * 用户自定义8
     */
    @TableField("user_def8")
    private String userDef8;
    /**
     * 用户自定义9
     */
    @TableField("user_def9")
    private String userDef9;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryHouseId() {
        return deliveryHouseId;
    }

    public void setDeliveryHouseId(String deliveryHouseId) {
        this.deliveryHouseId = deliveryHouseId;
    }

    public String getDeliveryHouseCode() {
        return deliveryHouseCode;
    }

    public void setDeliveryHouseCode(String deliveryHouseCode) {
        this.deliveryHouseCode = deliveryHouseCode;
    }

    public String getDeliveryHouseName() {
        return deliveryHouseName;
    }

    public void setDeliveryHouseName(String deliveryHouseName) {
        this.deliveryHouseName = deliveryHouseName;
    }

    public String getRecvHouseId() {
        return recvHouseId;
    }

    public void setRecvHouseId(String recvHouseId) {
        this.recvHouseId = recvHouseId;
    }

    public String getRecvHouseCode() {
        return recvHouseCode;
    }

    public void setRecvHouseCode(String recvHouseCode) {
        this.recvHouseCode = recvHouseCode;
    }

    public String getRecvHouseName() {
        return recvHouseName;
    }

    public void setRecvHouseName(String recvHouseName) {
        this.recvHouseName = recvHouseName;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getExpectOutDateLower() {
        return expectOutDateLower;
    }

    public void setExpectOutDateLower(Date expectOutDateLower) {
        this.expectOutDateLower = expectOutDateLower;
    }

    public Date getExpectOutDateUpper() {
        return expectOutDateUpper;
    }

    public void setExpectOutDateUpper(Date expectOutDateUpper) {
        this.expectOutDateUpper = expectOutDateUpper;
    }

    public Date getExpectInDateLower() {
        return expectInDateLower;
    }

    public void setExpectInDateLower(Date expectInDateLower) {
        this.expectInDateLower = expectInDateLower;
    }

    public Date getExpectInDateUpper() {
        return expectInDateUpper;
    }

    public void setExpectInDateUpper(Date expectInDateUpper) {
        this.expectInDateUpper = expectInDateUpper;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectSumQty() {
        return expectSumQty;
    }

    public void setExpectSumQty(BigDecimal expectSumQty) {
        this.expectSumQty = expectSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    public String getImportRemarks() {
        return importRemarks;
    }

    public void setImportRemarks(String importRemarks) {
        this.importRemarks = importRemarks;
    }

    public Date getImportTime() {
        return importTime;
    }

    public void setImportTime(Date importTime) {
        this.importTime = importTime;
    }

    public String getDataContent() {
        return dataContent;
    }

    public void setDataContent(String dataContent) {
        this.dataContent = dataContent;
    }

    public String getUserDef0() {
        return userDef0;
    }

    public void setUserDef0(String userDef0) {
        this.userDef0 = userDef0;
    }

    public String getUserDef1() {
        return userDef1;
    }

    public void setUserDef1(String userDef1) {
        this.userDef1 = userDef1;
    }

    public String getUserDef2() {
        return userDef2;
    }

    public void setUserDef2(String userDef2) {
        this.userDef2 = userDef2;
    }

    public String getUserDef3() {
        return userDef3;
    }

    public void setUserDef3(String userDef3) {
        this.userDef3 = userDef3;
    }

    public String getUserDef4() {
        return userDef4;
    }

    public void setUserDef4(String userDef4) {
        this.userDef4 = userDef4;
    }

    public String getUserDef5() {
        return userDef5;
    }

    public void setUserDef5(String userDef5) {
        this.userDef5 = userDef5;
    }

    public String getUserDef6() {
        return userDef6;
    }

    public void setUserDef6(String userDef6) {
        this.userDef6 = userDef6;
    }

    public String getUserDef7() {
        return userDef7;
    }

    public void setUserDef7(String userDef7) {
        this.userDef7 = userDef7;
    }

    public String getUserDef8() {
        return userDef8;
    }

    public void setUserDef8(String userDef8) {
        this.userDef8 = userDef8;
    }

    public String getUserDef9() {
        return userDef9;
    }

    public void setUserDef9(String userDef9) {
        this.userDef9 = userDef9;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ItfImplogHeader{" +
                ", id=" + id +
                ", ownerId=" + ownerId +
                ", ownerOrderNo=" + ownerOrderNo +
                ", type=" + type +
                ", orderDate=" + orderDate +
                ", deliveryHouseId=" + deliveryHouseId +
                ", deliveryHouseCode=" + deliveryHouseCode +
                ", deliveryHouseName=" + deliveryHouseName +
                ", recvHouseId=" + recvHouseId +
                ", recvHouseCode=" + recvHouseCode +
                ", recvHouseName=" + recvHouseName +
                ", carrierId=" + carrierId +
                ", carrierName=" + carrierName +
                ", transportMethod=" + transportMethod +
                ", plateNumber=" + plateNumber +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", expectOutDateLower=" + expectOutDateLower +
                ", expectOutDateUpper=" + expectOutDateUpper +
                ", expectInDateLower=" + expectInDateLower +
                ", expectInDateUpper=" + expectInDateUpper +
                ", uom=" + uom +
                ", expectSumQty=" + expectSumQty +
                ", lineCount=" + lineCount +
                ", sourceSystem=" + sourceSystem +
                ", sourceKey=" + sourceKey +
                ", sourceNo=" + sourceNo +
                ", status=" + status +
                ", remarks=" + remarks +
                ", importStatus=" + importStatus +
                ", importRemarks=" + importRemarks +
                ", importTime=" + importTime +
                ", dataContent=" + dataContent +
                ", userDef0=" + userDef0 +
                ", userDef1=" + userDef1 +
                ", userDef2=" + userDef2 +
                ", userDef3=" + userDef3 +
                ", userDef4=" + userDef4 +
                ", userDef5=" + userDef5 +
                ", userDef6=" + userDef6 +
                ", userDef7=" + userDef7 +
                ", userDef8=" + userDef8 +
                ", userDef9=" + userDef9 +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<ItfImplogLine> getItfImplogLineList() {
        return itfImplogLineList;
    }

    public void setItfImplogLineList(List<ItfImplogLine> itfImplogLineList) {
        this.itfImplogLineList = itfImplogLineList;
    }

    public void addItfImplogLine(ItfImplogLine itfImplogLine) {
        if (Objects.isNull(itfImplogLineList)) {
            this.itfImplogLineList = new ArrayList<>();
        }
        this.itfImplogLineList.add(itfImplogLine);

    }
}
