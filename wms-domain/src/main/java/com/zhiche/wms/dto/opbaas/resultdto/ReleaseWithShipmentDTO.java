package com.zhiche.wms.dto.opbaas.resultdto;

import com.zhiche.wms.domain.model.otm.OtmOrderRelease;

import java.io.Serializable;

public class ReleaseWithShipmentDTO extends OtmOrderRelease implements Serializable {
    private  String isCanSend;
    public String getIsCanSend() {
        return isCanSend;
    }

    public void setIsCanSend(String isCanSend) {
        this.isCanSend = isCanSend;
    }
}
