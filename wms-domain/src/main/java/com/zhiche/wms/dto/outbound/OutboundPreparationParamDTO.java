package com.zhiche.wms.dto.outbound;

import com.zhiche.wms.dto.base.PageVo;

import java.io.Serializable;

/**
 * 备料任务列表查询参数封装
 */
public class OutboundPreparationParamDTO extends PageVo implements Serializable {
    private String headSourceNo; //订单号
    private String headSourceKey; //运单号
    private String lineSourceNo; //调度指令号
    private String lineSourceKey; //子订单号
    private String preparationStatus;
    private String outShipStatus;
    private String startTime;
    private String endTime;
    private String noticeLineIds;
    private String noticeHeadId;
    private String planTime;
    private String storeHouseId;
    private String prepareLane;
    private String noticeHeadNo;
    private String ownerOrderNo;
    private String headId;
    private String[] lineIds;

    public String getHeadId() {
        return headId;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    public String[] getLineIds() {
        return lineIds;
    }

    public void setLineIds(String[] lineIds) {
        this.lineIds = lineIds;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getNoticeHeadNo() {
        return noticeHeadNo;
    }

    public void setNoticeHeadNo(String noticeHeadNo) {
        this.noticeHeadNo = noticeHeadNo;
    }

    public String getPrepareLane() {
        return prepareLane;
    }

    public void setPrepareLane(String prepareLane) {
        this.prepareLane = prepareLane;
    }

    public String getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(String storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getNoticeHeadId() {
        return noticeHeadId;
    }

    public void setNoticeHeadId(String noticeHeadId) {
        this.noticeHeadId = noticeHeadId;
    }

    public String getPlanTime() {
        return planTime;
    }

    public void setPlanTime(String planTime) {
        this.planTime = planTime;
    }

    public String getNoticeLineIds() {
        return noticeLineIds;
    }

    public void setNoticeLineIds(String noticeLineIds) {
        this.noticeLineIds = noticeLineIds;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHeadSourceNo() {
        return headSourceNo;
    }

    public void setHeadSourceNo(String headSourceNo) {
        this.headSourceNo = headSourceNo;
    }

    public String getHeadSourceKey() {
        return headSourceKey;
    }

    public void setHeadSourceKey(String headSourceKey) {
        this.headSourceKey = headSourceKey;
    }

    public String getLineSourceNo() {
        return lineSourceNo;
    }

    public void setLineSourceNo(String lineSourceNo) {
        this.lineSourceNo = lineSourceNo;
    }

    public String getLineSourceKey() {
        return lineSourceKey;
    }

    public void setLineSourceKey(String lineSourceKey) {
        this.lineSourceKey = lineSourceKey;
    }

    public String getPreparationStatus() {
        return preparationStatus;
    }

    public void setPreparationStatus(String preparationStatus) {
        this.preparationStatus = preparationStatus;
    }

    public String getOutShipStatus() {
        return outShipStatus;
    }

    public void setOutShipStatus(String outShipStatus) {
        this.outShipStatus = outShipStatus;
    }
}
