package com.zhiche.wms.dto.outbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OutboundNoticeLineParamDTO implements Serializable {

    /**
     * 入库通知单头键
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long headerId;
    /**
     * 序号
     */
    private String seq;

    /**
     * 行来源唯一键
     */
    private String lineSourceKey;
    /**
     * 行来源单据号
     */
    private String lineSourceNo;
    /**
     * 货主
     */
    private String lineOwnerId;
    /**
     * 货主单号
     */
    private String lineOwnerOrderNo;
    /**
     * 物料ID
     */
    private String materielId;
    /**
     * 物料代码
     */
    private String materielCode;
    /**
     * 物料名称
     */
    private String materielName;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计数量
     */
    private BigDecimal expectQty;
    /**
     * 预计净重
     */
    private BigDecimal expectNetWeight;
    /**
     * 预计毛重
     */
    private BigDecimal expectGrossWeight;
    /**
     * 预计体积
     */
    private BigDecimal expectGrossCubage;
    /**
     * 预计件数
     */
    private BigDecimal expectPackedCount;
    /**
     * 出库数量
     */
    private BigDecimal outboundQty;
    /**
     * 出库净重
     */
    private BigDecimal outboundNetWeight;
    /**
     * 出库毛重
     */
    private BigDecimal outboundGrossWeight;
    /**
     * 出库体积
     */
    private BigDecimal outboundGrossCubage;
    /**
     * 出库件数
     */
    private BigDecimal ourtboundPackedCount;
    /**
     * 批号0
     */
    private String lotNo0;
    /**
     * 批号1
     */
    private String lotNo1;
    /**
     * 批号2
     */
    private String lotNo2;
    /**
     * 批号3
     */
    private String lotNo3;
    /**
     * 批号4
     */
    private String lotNo4;
    /**
     * 批号5
     */
    private String lotNo5;
    /**
     * 批号6
     */
    private String lotNo6;
    /**
     * 批号7
     */
    private String lotNo7;
    /**
     * 批号8
     */
    private String lotNo8;
    /**
     * 批号9
     */
    private String lotNo9;
    /**
     * 状态(10:未入库,20:部分入库,30:全部入库,40:关闭入库,40:取消)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;



    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getLineSourceKey() {
        return lineSourceKey;
    }

    public void setLineSourceKey(String lineSourceKey) {
        this.lineSourceKey = lineSourceKey;
    }

    public String getLineSourceNo() {
        return lineSourceNo;
    }

    public void setLineSourceNo(String lineSourceNo) {
        this.lineSourceNo = lineSourceNo;
    }

    public String getLineOwnerId() {
        return lineOwnerId;
    }

    public void setLineOwnerId(String lineOwnerId) {
        this.lineOwnerId = lineOwnerId;
    }

    public String getLineOwnerOrderNo() {
        return lineOwnerOrderNo;
    }

    public void setLineOwnerOrderNo(String lineOwnerOrderNo) {
        this.lineOwnerOrderNo = lineOwnerOrderNo;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectQty() {
        return expectQty;
    }

    public void setExpectQty(BigDecimal expectQty) {
        this.expectQty = expectQty;
    }

    public BigDecimal getExpectNetWeight() {
        return expectNetWeight;
    }

    public void setExpectNetWeight(BigDecimal expectNetWeight) {
        this.expectNetWeight = expectNetWeight;
    }

    public BigDecimal getExpectGrossWeight() {
        return expectGrossWeight;
    }

    public void setExpectGrossWeight(BigDecimal expectGrossWeight) {
        this.expectGrossWeight = expectGrossWeight;
    }

    public BigDecimal getExpectGrossCubage() {
        return expectGrossCubage;
    }

    public void setExpectGrossCubage(BigDecimal expectGrossCubage) {
        this.expectGrossCubage = expectGrossCubage;
    }

    public BigDecimal getExpectPackedCount() {
        return expectPackedCount;
    }

    public void setExpectPackedCount(BigDecimal expectPackedCount) {
        this.expectPackedCount = expectPackedCount;
    }

    public BigDecimal getOutboundQty() {
        return outboundQty;
    }

    public void setOutboundQty(BigDecimal outboundQty) {
        this.outboundQty = outboundQty;
    }

    public BigDecimal getOutboundNetWeight() {
        return outboundNetWeight;
    }

    public void setOutboundNetWeight(BigDecimal outboundNetWeight) {
        this.outboundNetWeight = outboundNetWeight;
    }

    public BigDecimal getOutboundGrossWeight() {
        return outboundGrossWeight;
    }

    public void setOutboundGrossWeight(BigDecimal outboundGrossWeight) {
        this.outboundGrossWeight = outboundGrossWeight;
    }

    public BigDecimal getOutboundGrossCubage() {
        return outboundGrossCubage;
    }

    public void setOutboundGrossCubage(BigDecimal outboundGrossCubage) {
        this.outboundGrossCubage = outboundGrossCubage;
    }

    public BigDecimal getOurtboundPackedCount() {
        return ourtboundPackedCount;
    }

    public void setOurtboundPackedCount(BigDecimal ourtboundPackedCount) {
        this.ourtboundPackedCount = ourtboundPackedCount;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}
