package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.opbaas.ExceptionRegisterPictureMapper;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegisterPicture;
import com.zhiche.wms.dto.opbaas.resultdto.PictureWithExcpDescDTO;
import com.zhiche.wms.service.opbaas.IExceptionRegisterPictureService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 异常图片 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-06-04
 */
@Service
public class ExceptionRegisterPictureServiceImpl extends ServiceImpl<ExceptionRegisterPictureMapper, ExceptionRegisterPicture> implements IExceptionRegisterPictureService {

    @Override
    public List<PictureWithExcpDescDTO> listPicsAndDesc(EntityWrapper<ExceptionRegisterPicture> picEW) {
        return baseMapper.listPicsAndDesc(picEW);
    }
}
