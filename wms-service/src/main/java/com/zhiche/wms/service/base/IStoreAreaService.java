package com.zhiche.wms.service.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 库区配置 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IStoreAreaService extends IService<StoreArea> {

    /**
     * 查询库区信息
     */
    Page<StoreArea> queryByPage(Page<StoreArea> page);

    /**
     * 通过code查询数据数
     */
    int getCountByCodeName(StoreArea storeArea);

    boolean updateStoreArea(StoreArea storeArea);
}
