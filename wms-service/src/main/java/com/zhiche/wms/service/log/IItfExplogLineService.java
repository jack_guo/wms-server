package com.zhiche.wms.service.log;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.log.ItfExplogLine;

/**
 * <p>
 * 接口导出日志明细 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface IItfExplogLineService extends IService<ItfExplogLine> {

}
