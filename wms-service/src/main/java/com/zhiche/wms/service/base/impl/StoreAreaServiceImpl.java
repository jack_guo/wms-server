package com.zhiche.wms.service.base.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.domain.mapper.base.StoreAreaMapper;
import com.zhiche.wms.domain.model.base.StoreArea;
import com.zhiche.wms.service.base.IStoreAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 库区配置 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
@Service
public class StoreAreaServiceImpl extends ServiceImpl<StoreAreaMapper, StoreArea> implements IStoreAreaService {

    @Override
    public Page<StoreArea> queryByPage(Page<StoreArea> page) {
        if (Objects.isNull(page) || Objects.isNull(page.getCondition())) {
            throw new BaseException("参数不能为空");
        }
        List<StoreArea> areaList = null;
        Wrapper<StoreArea> ew = buildCondition(page.getCondition());
        areaList = this.baseMapper.queryByPage(page, ew);
        page.setRecords(areaList);
        return page;
    }

    @Override
    public int getCountByCodeName(StoreArea storeArea) {
        if (Objects.isNull(storeArea) || Objects.isNull(storeArea)) {
            throw new BaseException("参数不能为空");
        }
        if (storeArea.getStoreHouseId() == null || StringUtils.isBlank(storeArea.getStoreHouseId().toString())) {
            throw new BaseException("仓库id不能为空");
        }
        if (StringUtils.isBlank(storeArea.getCode())) {
            throw new BaseException("库区编码不能为空");
        }
        if (StringUtils.isBlank(storeArea.getName())) {
            throw new BaseException("库区名称不能为空");
        }
        Wrapper<StoreArea> ew = new EntityWrapper<>();
        ew.eq("store_house_id", storeArea.getStoreHouseId().toString());
        ew.andNew().eq("code", storeArea.getCode()).or().eq("name", storeArea.getName());
        int count = this.baseMapper.getCountByCode(ew);
        return count;
    }

    @Override
    public boolean updateStoreArea(StoreArea storeArea) {
        Wrapper<StoreArea> ew = new EntityWrapper<>();
        ew.eq("name",storeArea.getName()).eq("store_house_id",storeArea.getStoreHouseId())
                .ne("id",storeArea.getId());
        int selectCount = selectCount(ew);
        if (selectCount > 0){
            throw new BaseException("库区名称已存在");
        }
        boolean updateById = updateById(storeArea);
        return updateById;
    }

    private Wrapper<StoreArea> buildCondition(Map<String, Object> condition) {
        Wrapper<StoreArea> ew = new EntityWrapper<>();
        if (condition.containsKey("houseId") && Objects.nonNull(condition.get("houseId"))) {
            ew.eq("store_house_id", condition.get("houseId").toString());
        }
        if (condition.containsKey("name") && Objects.nonNull(condition.get("name"))) {
            ew.like("name", condition.get("name").toString());
        }
        if (condition.containsKey("type") && Objects.nonNull(condition.get("type"))) {
            ew.eq("type", condition.get("type").toString());
        }
        if (condition.containsKey("status") && Objects.nonNull(condition.get("status"))) {
            ew.eq("status", condition.get("status").toString());
        }
        ew.orderBy("id");
        return ew;
    }

}
