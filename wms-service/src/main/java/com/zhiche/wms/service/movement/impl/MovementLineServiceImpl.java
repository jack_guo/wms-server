package com.zhiche.wms.service.movement.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.movement.MovementLineMapper;
import com.zhiche.wms.domain.model.movement.MovementLine;
import com.zhiche.wms.service.movement.IMovementLineService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 移位明细 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@Service
public class MovementLineServiceImpl extends ServiceImpl<MovementLineMapper, MovementLine> implements IMovementLineService {

}
