package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/20.
 */
public class ShipType {

    /**
     * 通知入库
     */
    public static final String NOTICE_SHIP = "10"; //通知入库

    /**
     * 盲出出库
     */
    public static final String BLIND_SHIP = "20"; //盲出出库

    /**
     * 维修出库
     */
    public static final String REPAIR_SHIP = "30"; //维修出库

    /**
     * 借车出库
     */
    public static final String BRR_SHIP = "40"; //借车出库
}
