package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/17.
 */
public class YesOrNo {
    /**
     * 是
     */
    public static final String YES = "1";

    /**
     * 否
     */
    public static final String NO = "0";
}
