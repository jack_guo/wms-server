package com.zhiche.wms.service.common;


import com.zhiche.wms.dto.opbaas.resultdto.NodeProcessInfoDTO;

/**
 * <p>
 * 业务节点处理 服务类
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
public interface INodeProcessService {

    /**
     * 获取运单相关信息
     */
    NodeProcessInfoDTO queryNodeInfo(NodeProcessInfoDTO dto);
}
