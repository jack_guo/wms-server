package com.zhiche.wms.service.stockadjust.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.stockadjust.StockAdjustLineMapper;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustLine;
import com.zhiche.wms.service.stockadjust.IStockAdjustLineService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存调整明细 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@Service
public class StockAdjustLineServiceImpl extends ServiceImpl<StockAdjustLineMapper, StockAdjustLine> implements IStockAdjustLineService {

}
