package com.zhiche.wms.service.stock;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stock.SkuStore;

/**
 * <p>
 * 停放位置信息 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-10-19
 */
public interface ISkuStoreService extends IService<SkuStore> {

}
