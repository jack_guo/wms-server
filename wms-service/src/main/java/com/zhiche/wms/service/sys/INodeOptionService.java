package com.zhiche.wms.service.sys;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.base.NodeOption;

public interface INodeOptionService extends IService<NodeOption> {

}
