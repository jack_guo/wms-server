package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceHandlingParamDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExResultDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 异常登记 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IExceptionRegisterService extends IService<ExceptionRegister> {

    ArrayList<ExResultDTO> getExceptionCountByTaskId(ExceptionRegisterParamDTO dto);

    void updateNoteException(ExceptionRegisterParamDTO dto);

    ArrayList<ExResultDTO> updateExceptionInfoForScan(ExceptionRegisterParamDTO dto);

    List<ExResultDTO> getExCount(ExceptionRegisterParamDTO dto);

    List<ExResultDTO> getExceptionPicUrl(ExceptionRegisterParamDTO dto);

    /**
     * 异常列表查询
     */
    Page<ExResultDTO> queryExceptionList(Page<ExResultDTO> page);

    /**
     * 异常列表导出
     */
    List<ExResultDTO> queryExportExcpList(Page<ExResultDTO> page);

    /**
     * 异常列表明细查询
     */
    Page<ExResultDTO> queryExceptionDetail(Page<ExResultDTO> page);

    /**
     *异常处理
     */
    void exceptionHandling(ExceHandlingParamDTO exceHandlingParamDTO);
}
