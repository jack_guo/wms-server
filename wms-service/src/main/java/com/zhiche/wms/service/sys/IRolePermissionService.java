package com.zhiche.wms.service.sys;

import com.zhiche.wms.domain.model.sys.RolePermission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 角色权限关联表 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
public interface IRolePermissionService extends IService<RolePermission> {

}
