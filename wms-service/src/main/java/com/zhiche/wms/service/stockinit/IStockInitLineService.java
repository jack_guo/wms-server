package com.zhiche.wms.service.stockinit;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stockinit.StockInitLine;

/**
 * <p>
 * 期初库存明细 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IStockInitLineService extends IService<StockInitLine> {

}
