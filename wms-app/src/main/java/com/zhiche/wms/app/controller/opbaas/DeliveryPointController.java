package com.zhiche.wms.app.controller.opbaas;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 发车点配置 前端控制器
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@RestController
@RequestMapping("model/deliveryPoint")
public class DeliveryPointController {

}

