package com.zhiche.wms.admin.test.inbound;

import com.alibaba.fastjson.JSONObject;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.domain.mapper.inbound.InboundNoticeHeaderMapper;
import com.zhiche.wms.domain.mapper.inbound.InboundNoticeLineMapper;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class InboundNoticeTest {

    //@Autowired
    //private TestRestTemplate testRestTemplate;
    @Autowired
    private InboundNoticeHeaderMapper headerMapper;
    @Autowired
    private InboundNoticeLineMapper lineMapper;

    @Test
    public void testGetStr() {
        List<InboundNoticeHeader> headers = headerMapper.selectList(null);
        List<InboundNoticeLine> lines = lineMapper.selectList(null);
        String hStr = JSONObject.toJSONString(headers);
        String lStr = JSONObject.toJSONString(lines);
    }

}
