package com.zhiche.wms.admin.vo.sys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2017/12/10.
 */
public class ElTreeVO implements Serializable {

    private static final long serialVersionUID = 508592016941085741L;
    private Integer id;
    private String  label;
    private List<ElTreeVO> children = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<ElTreeVO> getChildren() {
        return children;
    }

    public void setChildren(List<ElTreeVO> children) {
        this.children = children;
    }

    public void addChildren(ElTreeVO child){
        if(Objects.equals(children,null)) children = new ArrayList<>();
        this.children.add(child);
    }

}
