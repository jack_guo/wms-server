package com.zhiche.wms.admin.surpports.webservice;

import com.zhiche.wms.service.webserivce.OpBuyShipmentWsService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * <p>
 * webservice  配置
 * </p>
 */
@Configuration
public class CxfConfigurationBean {

    @Autowired
    private Bus bus;

    @Autowired
    private OpBuyShipmentWsService buyShipmentWsService;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, buyShipmentWsService);
        endpoint.publish("/buyShipmentWsService");
        return endpoint;
    }
}
