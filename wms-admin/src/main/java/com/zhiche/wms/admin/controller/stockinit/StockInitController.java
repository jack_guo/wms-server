package com.zhiche.wms.admin.controller.stockinit;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.stockinit.StockInitQueryListResultDTO;
import com.zhiche.wms.service.stockinit.IStockInitHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 期初库存表现层
 */
@RestController
@RequestMapping("/stockInit")
public class StockInitController {

    @Autowired
    private IStockInitHeaderService iStockInitHeaderService;


    /**
     * 导入期初库存 保存状态status20
     */
    @PostMapping("/importStockInit")
    public RestfulResponse<Map<String, JSONArray>> saveImportStockInit(@RequestBody String data) {
        Map<String, JSONArray> result = iStockInitHeaderService.saveImportStockInitNew(data);
        return new RestfulResponse<>(0,"请求成功",result);
    }

    /**
     * 期初库存列表查询
     */
    @PostMapping("/getStockInitList")
    public RestfulResponse<Page<StockInitQueryListResultDTO>> getStockInitList(@RequestBody Page<StockInitQueryListResultDTO> page) {
        Page<StockInitQueryListResultDTO> data = iStockInitHeaderService.queryStockPage(page);
        return new RestfulResponse<>(0, "查询成功", data);
    }
}
