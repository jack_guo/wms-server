package com.zhiche.wms.core.supports.enums;

public enum SysSourceEnum {

    SCHEDULE("10", "SCHEDULE", "定时任务执行触发"),
    OMS_CREATE("20", "OMS", "oms系统触发"),
    OTM_CREATE("21", "OTM", "OTM系统触发"),
    TMS_CREATE("30", "TMS", "tms系统触发"),
    JM_CREATE("40", "JUNMA", "君马系统触发"),
    MQ("50", "MQ", "mq推送"),
    REST_MODIFY("60", "REST", "rest接口修改数据"),
    NORMAL_TYPE("61", "NORMAL", "初始状态"),
    MODIFY_TYPE("62", "MODIFY", "正常修改"),
    DELETE_TYPE("63", "DELETE", "修改删除"),
    ADD_TYPE("64", "ADD", "修改新增"),
    IMPORT("70", "IMPORT", "excel导入"),
    WEBSERVICE("75", "WEBSERVICE", "webservice 接口获取"),
    AUTO("80","AUTO","系统自动入库"),
    HONEYWELL("90","HONEYWELL","霍尼出库入库");

    private final String code;
    private final String name;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    public String getName() {
        return name;
    }

    SysSourceEnum(String code, String name, String detail) {
        this.code = code;
        this.name = name;
        this.detail = detail;
    }


}
